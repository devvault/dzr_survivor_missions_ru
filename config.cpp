class CfgPatches
{

    class dzr_survivor_missions_ru
	{
		requiredAddons[] = {"DZ_Data"};
units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
    class dzr_survivor_missions_ru
	{
		type = "mod";
		author = "DayZRussia.com";
		description = "Translated default Chernarus missions to Russian";
		dir = "dzr_survivor_missions_ru";
		name = "DZR Survivor Misson RU translation";
		inputs = "dzr_survivor_missions_ru/Data/Inputs.xml";
		dependencies[] = {"Game", "World", "Mission"};
		class defs
        {
			class gameScriptModule
			{
				files[] = {"dzr_survivor_missions_ru/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_survivor_missions_ru/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_survivor_missions_ru/5_Mission"};
			};

        };
	};
};